# Geekstuff.dev / Devcontainers / Features / TF Backend Gitlab

This devcontainer adds direnv, make scripts and variables to easily
use Gitlab terraform backend.

## How to use

This feature is used at a few levels.

### In devcontainer.json

In your `.devcontainer/devcontainer.json`, add this feature elements:

```json
{
    "name": "my devcontainer",
    "image": "debian:bullseye",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/basics": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/direnv": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/terraform": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/tf-backend-gitlab": {
            "instance_host": "gitlab.com",
            "state_project_id": "",
            "state_prefix": ""
        }
    }
}
```

- This feature requires the other features here.
- The options shown are the defaults that you can omit or override.

### In your terraform folder(s)

The feature comes with a direnv to export a bunch of TF http backend ENV vars, and make script to make things as simple
as having nothing related to the HTTP backend to define.

#### Terraform backend

```
terraform {
  backend "http" {
  }
}
```

#### Few ways to provide username and password

There are at least 2 methods to do this:

##### Method 1: git ignored local file

In this method each user must write its own gitlab state username and password,
in a local git ignored file.

###### direnv

In your terraform folder, in project root or subfolder, create a `.envrc`
file with this content:

```
TF_STATE_NAME=${PWD##*/}
source_env_if_exists .local.env
source_env ${TF_DIRENV}/main.env
```

Depending if on project root or subfolder, you may want to use `.local.env` path.

###### gitignore

The `.local.env` file must be also added to your existing or new `.gitignore`
file. Example:

```
/.local.env
```

###### .local.env file content

In method no. 1, gitlab username and password to write TF state in gitlab
is written here and must have api scope. It can be a personal token,
a project token or a group token.

```
GITLAB_STATE_USERNAME="username"
GITLAB_STATE_TOKEN="password"
```

NOTE: Don't forget to gitignore this file within the project.

##### Method 2: hashicorp vault

In this method, the gitlab tf state username and password is stored in vault
and retrieved instead of the local file.

###### How to setup

Simplest way is to:

- Use vault-cli vscode feature
- Set VAULT_ADDR and VAULT_NAMESPACE as containerEnv vars in devcontainer.json
- Maybe add some simple Makefile job to login to vault

###### How to use

###### direnv

In your terraform folder, in project root or subfolder, create a `.envrc`
file with this content:

```
TF_STATE_NAME=${PWD##*/}

# vault path to secret, and the secret field for password
TFSTATE_VAULT_SECRET=kv/example-path/gitlab/tfstate_credentials
TFSTATE_VAULT_SECRET_FIELD=GITLAB_TOKEN

source_env ${TF_DIRENV}/main.env
```

#### Optional Makefile

In your tf folder, create a `Makefile` with this example content:

```
#!make

all: tf-init tf-apply

include ${TF_MAKE}/tf.mk
```

Now can just run `make` in that folder along with other similar commands.
